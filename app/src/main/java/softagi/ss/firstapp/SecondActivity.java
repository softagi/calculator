package softagi.ss.firstapp;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        TextView textView = findViewById(R.id.profile_txt);
        ImageView imageView = findViewById(R.id.image);

        userModel model = (userModel) getIntent().getSerializableExtra("user");

        if (model.getImage() != 0)
        {
            imageView.setImageResource(model.getImage());
        }
        textView.setText(model.getName() + "\n" + model.getMobile() + "\n" + model.getAddress());
    }
}
