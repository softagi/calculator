package softagi.ss.firstapp;

import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
{
    private RecyclerView recyclerView;
    private List<userModel> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
        initList();
        recyclerView.setAdapter(new userAdapter(list));
        recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL));
    }

    private void initList()
    {
        list.add(new userModel("abdullah","0165135613","nasr city", R.drawable.android));
        list.add(new userModel("hema","0165135613","nasr city", R.drawable.app));
        list.add(new userModel("Ridge","0165135613","nasr city", R.drawable.backend));
        list.add(new userModel("karem","0165135613","nasr city", R.drawable.idea));
        list.add(new userModel("ali","0165135613","nasr city", R.drawable.mosque));
        list.add(new userModel("bassam","0165135613","nasr city", R.drawable.android));
        list.add(new userModel("alaa","0165135613","nasr city" , R.drawable.android));

        list.add(new userModel("middle","0165135613","nasr city" , R.drawable.mosque));
        list.add(new userModel("hema","0165135613","nasr city" , R.drawable.android));
        list.add(new userModel("ridge","0165135613","nasr city" , R.drawable.mosque));
        list.add(new userModel("karem","0165135613","nasr city" , R.drawable.android));
        list.add(new userModel("ali","0165135613","nasr city" , R.drawable.android));
        list.add(new userModel("bassam","0165135613","nasr city" , R.drawable.mosque));
        list.add(new userModel("alaa","0165135613","nasr city" , R.drawable.android));

        list.add(new userModel("last","0165135613","nasr city" , R.drawable.android));
    }

    private void initViews()
    {
        recyclerView = findViewById(R.id.user_recycler);
    }

    class userAdapter extends RecyclerView.Adapter<userAdapter.userVH>
    {
        List<userModel> list;
        int po;

        userAdapter(List<userModel> list)
        {
            this.list = list;
        }

        @NonNull
        @Override
        public userVH onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
        {
            View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.user_item, viewGroup, false);
            return new userVH(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final userVH userVH, final int i)
        {
            final userModel model = list.get(i);

            String name = model.getName();
            String mobile = model.getMobile();
            String address = model.getAddress();
            int image = model.getImage();

            userVH.username_txt.setText(name);
            userVH.mobile_txt.setText(mobile);
            userVH.address_txt.setText(address);
            userVH.user_image.setImageResource(image);

            userVH.itemView.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    po = i;
                    Log.d("pop up start", "done");
                    popUp(userVH.itemView, model, po);
                }
            });
        }

        @Override
        public int getItemCount()
        {
            return list.size();
        }

        class userVH extends RecyclerView.ViewHolder
        {
            TextView username_txt,mobile_txt,address_txt;
            ImageView user_image;

            userVH(@NonNull View itemView)
            {
                super(itemView);

                username_txt = itemView.findViewById(R.id.username_txt);
                mobile_txt = itemView.findViewById(R.id.mobile_txt);
                address_txt = itemView.findViewById(R.id.address_txt);
                user_image = itemView.findViewById(R.id.user_image);
            }
        }

        void popUp(View view, final userModel model, final int position)
        {
            //Creating the instance of PopupMenu
            PopupMenu popup = new PopupMenu(MainActivity.this, view);
            //Inflating the Popup using xml file
            popup
                    .getMenuInflater()
                    .inflate(R.menu.pop_up_menu, popup.getMenu());

            //registering popup with OnMenuItemClickListener
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener()
            {
                public boolean onMenuItemClick(MenuItem item)
                {
                    switch (item.getItemId())
                    {
                        case R.id.details:
                            Intent intent = new Intent(getApplicationContext(), SecondActivity.class);
                            intent.putExtra("user", model);
                            startActivity(intent);
                            break;
                        case R.id.delete:
                            list.remove(position);
                            notifyItemRemoved(position);
                            Log.d("position is", String.valueOf(position));
                            Toast.makeText(getApplicationContext(), "delete", Toast.LENGTH_SHORT).show();
                            break;
                    }
                    return true;
                }
            });

            popup.show(); //showing popup menu
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.delete_all:
                Toast.makeText(getApplicationContext(), "delete all", Toast.LENGTH_SHORT).show();
                break;
            case R.id.search:
                Toast.makeText(getApplicationContext(), "search", Toast.LENGTH_SHORT).show();
                break;
        }
        return true;
    }
}