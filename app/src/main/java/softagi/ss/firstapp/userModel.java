package softagi.ss.firstapp;

import java.io.Serializable;

public class userModel implements Serializable
{
    private String name;
    private String mobile;
    private String address;
    private int image;

    public userModel(String name, String mobile, String address, int image) {
        this.name = name;
        this.mobile = mobile;
        this.address = address;
        this.image = image;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}