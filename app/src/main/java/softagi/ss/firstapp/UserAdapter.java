package softagi.ss.firstapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserVH>
{
    private List<userModel> models;
    private Context context;

    public UserAdapter(List<userModel> models, Context context)
    {
        this.models = models;
        this.context = context;
    }

    @NonNull
    @Override
    public UserVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(context).inflate(R.layout.user_item, parent, false);
        return new UserVH(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UserVH holder, int position)
    {
        final userModel model = models.get(position);

        String name = model.getName();
        String mobile = model.getMobile();
        String address = model.getAddress();
        int image = model.getImage();

        holder.username_txt.setText(name);
        holder.mobile_txt.setText(mobile);
        holder.address_txt.setText(address);
        holder.user_image.setImageResource(image);

        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                /*Intent intent = new Intent(context, SecondActivity.class);
                intent.putExtra("user", model);
                context.startActivity(intent);*/
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return models.size();
    }

    class UserVH extends RecyclerView.ViewHolder
    {
        TextView username_txt,mobile_txt,address_txt;
        ImageView user_image;

        public UserVH(@NonNull View itemView)
        {
            super(itemView);

            username_txt = itemView.findViewById(R.id.username_txt);
            mobile_txt = itemView.findViewById(R.id.mobile_txt);
            address_txt = itemView.findViewById(R.id.address_txt);
            user_image = itemView.findViewById(R.id.user_image);
        }
    }
}